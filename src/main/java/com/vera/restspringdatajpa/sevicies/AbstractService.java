/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.sevicies;

import com.vera.restspringdatajpa.model.Prod;
import java.util.List;

/**
 *
 * @author v.e.doronin
 */
public interface AbstractService<T> {
    public List<T> getAll();

    public List<T> findByName(String name);
    
    public void delete(Integer id);
    
    public long save(T object);

    public T get(long id);

    public void update(long id, T object);
    
    public T getById(Integer id);

    public List<T> join();
   
}

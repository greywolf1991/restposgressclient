/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.sevicies;

import com.vera.restspringdatajpa.model.Prod;
import com.vera.restspringdatajpa.model.ServiceCentr;
import com.vera.restspringdatajpa.repository.AbstractRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author v.e.doronin
 */
@Service("centrService")
@Transactional
public class CentrServive  implements AbstractService<ServiceCentr>{

    @Autowired
    @Qualifier("centrRepository")
    AbstractRepository repository;
    
    @Override
    public List<ServiceCentr> getAll() {
        return repository.list();
    }

    @Override
    public List<ServiceCentr> findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long save(ServiceCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServiceCentr get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, ServiceCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServiceCentr getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ServiceCentr> join() {
        return repository.join();
    }
    
}

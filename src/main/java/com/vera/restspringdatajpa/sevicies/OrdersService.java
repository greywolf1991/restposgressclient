/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.sevicies;

import com.vera.restspringdatajpa.model.Orders;
import com.vera.restspringdatajpa.model.Prod;
import com.vera.restspringdatajpa.repository.AbstractRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author v.e.doronin
 */
@Service("ordersService")
@Transactional
public class OrdersService  implements AbstractService<Orders>{

    @Autowired
    @Qualifier("ordersRepository")
    AbstractRepository repository;
    
    @Transactional
    @Override
    public List<Orders> getAll() {
        return repository.list();
    }
    
    
    @Transactional
    @Override
    public List<Orders> findByName(String name) {
        return repository.findByName(name);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long save(Orders object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Orders get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, Orders object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Orders getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Orders> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

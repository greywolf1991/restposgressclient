/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.sevicies;

import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.model.Prod;
import com.vera.restspringdatajpa.repository.AbstractRepository;
import com.vera.restspringdatajpa.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Wera
 */
@Service("customerService")
@Transactional
public class CustomerService implements AbstractService<Customer> {
        @Autowired
        @Qualifier("customerRepository")
	AbstractRepository repository;

	@Transactional
        @Override
	public List<Customer> getAll() {
		return (List<Customer>) repository.list();
	}

	@Transactional
        @Override
	public List<Customer> findByName(String name) {
		return repository.findByName(name);
	}

	@Transactional
        @Override
	public void delete(Integer customerId) {
		repository.delete(customerId);
	}

	//@Transactional
	//public boolean addCustomer(Customer customer) {
	//	return customerRepository.save(customer) != null;
	//}

	//@Transactional
	//public boolean updateCustomer(Customer customer) {
	//	return customerRepository.save(customer) != null;
	//}

    @Override
    public long save(Customer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, Customer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Customer> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

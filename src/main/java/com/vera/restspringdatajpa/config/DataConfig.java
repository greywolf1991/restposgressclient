/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.config;

/**
 *
 * @author Wera
 */


import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernatePersistenceProvider;
//import org.hibernate.ejb.HibernatePersistence;
//import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;




@Configuration
@ComponentScan(basePackages = {
    "com.vera.restspringdatajpa.logical", "com.vera.restspringdatajpa.sevicies"})
//@EnableJpaRepositories("com.vera.restspringdatajpa.repository")
@EnableTransactionManagement
@PropertySource("classpath:database.properties")
public class DataConfig {

    public DataConfig() {
        
    }

    @Autowired
    Environment environment;

    private final String URL = "url";
    private final String USER = "user";
    private final String DRIVER = "driver";
    private final String PASSWORD = "password";
    private final String PROPERTY_SHOW_SQL = "hibernate.show_sql";
    private final String PROPERTY_DIALECT = "spring.jpa.properties.hibernate.dialect";
    private final String PROPERTY_UPDATE = "spring.jpa.hibernate.ddl-auto";
    private final String CURRENT_SESSION_CONTEXT_CLASS = "hibernate.current.session.context.class";
    public static final String PERSISTANCE_UNIT_NAME = "com.vera_RestSpringDataJPA_war_1.0-SNAPSHOTPU";

//    @Bean
//    public LocalSessionFactoryBean sessionFactory() {
//        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
//        sessionFactory.setDataSource(dataSource());
//        sessionFactory.setPackagesToScan("com.vera.restspringdatajpa");
//        sessionFactory.setHibernateProperties(hibernateProperties());
//        return sessionFactory;
//     }
    
//    @Bean
//    SessionFactory sessionFactory(@Qualifier("entityManagerFactory") EntityManagerFactory emf) {
//    return emf.unwrap(SessionFactory.class);
//    }
    
    @Bean
    DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = 
                new DriverManagerDataSource();
        driverManagerDataSource.setUrl(environment.getProperty(URL));
        driverManagerDataSource.setUsername(environment.getProperty(USER));
        driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
        driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
        
        return driverManagerDataSource;
    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lfb =
                new LocalContainerEntityManagerFactoryBean();
        lfb.setDataSource(dataSource());
        lfb.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        lfb.setPackagesToScan(new String[] {"com.vera.restspringdatajpa.repository" , "com.vera.restspringdatajpa.model"
                                             , "com.vera.restspringdatajpa.services", "com.vera.restspringdatajpa.controller"});
        lfb.setJpaProperties(hibernateProperties());
        //lfb.setPersistenceXmlLocation("jpa/my-persistence.xml");
        lfb.setPersistenceUnitName(PERSISTANCE_UNIT_NAME);
        return lfb;
    }

    //@Bean
    Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty
        (PROPERTY_DIALECT, environment.getProperty(PROPERTY_DIALECT));
        properties.setProperty
        (PROPERTY_SHOW_SQL, environment.getProperty(PROPERTY_SHOW_SQL));
	properties.setProperty(PROPERTY_UPDATE, environment.getProperty(PROPERTY_UPDATE));
	properties.setProperty(CURRENT_SESSION_CONTEXT_CLASS, environment.getRequiredProperty(CURRENT_SESSION_CONTEXT_CLASS));
        return properties;
    }

//    @Bean
//    @Autowired
//    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
//      HibernateTransactionManager txManager  = new HibernateTransactionManager();
//      txManager.setSessionFactory(sessionFactory);
//      return txManager;
//   }
    @Bean
    PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = 
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                entityManagerFactory().getObject());
        return transactionManager;
    }
   


   
    
  
 

}
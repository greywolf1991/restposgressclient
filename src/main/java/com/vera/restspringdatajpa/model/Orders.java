/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author v.e.doronin
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o")
    , @NamedQuery(name = "Orders.findByNumber", query = "SELECT o FROM Orders o WHERE o.ordersPK.number = :number")
    , @NamedQuery(name = "Orders.findByDateOfReceipt", query = "SELECT o FROM Orders o WHERE o.dateOfReceipt = :dateOfReceipt")
    , @NamedQuery(name = "Orders.findByDateOfExec", query = "SELECT o FROM Orders o WHERE o.dateOfExec = :dateOfExec")
    , @NamedQuery(name = "Orders.findByVarranty", query = "SELECT o FROM Orders o WHERE o.varranty = :varranty")
    , @NamedQuery(name = "Orders.findByTypeRepair", query = "SELECT o FROM Orders o WHERE o.typeRepair = :typeRepair")
    , @NamedQuery(name = "Orders.findByPrice", query = "SELECT o FROM Orders o WHERE o.price = :price")
    , @NamedQuery(name = "Orders.findByFio", query = "SELECT o FROM Orders o WHERE o.ordersPK.fio = :fio")
    , @NamedQuery(name = "Orders.findByPhone", query = "SELECT o FROM Orders o WHERE o.ordersPK.phone = :phone")
    , @NamedQuery(name = "Orders.findByNameProd", query = "SELECT o FROM Orders o WHERE o.nameProd = :nameProd")
    , @NamedQuery(name = "Orders.findBySnProd", query = "SELECT o FROM Orders o WHERE o.snProd = :snProd")
    , @NamedQuery(name = "Orders.findByNumberSc", query = "SELECT o FROM Orders o WHERE o.ordersPK.numberSc = :numberSc")
    , @NamedQuery(name = "Orders.findByNameSc", query = "SELECT o FROM Orders o WHERE o.ordersPK.nameSc = :nameSc")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdersPK ordersPK;
    @Column(name = "date_of_receipt")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfReceipt;
    @Column(name = "date_of_exec")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfExec;
    @Basic(optional = false)
    @NotNull
    @Column(name = "varranty")
    private BigInteger varranty;
    @Size(max = 100)
    @Column(name = "type_repair")
    private String typeRepair;
    @Column(name = "price")
    private Integer price;
    @Size(max = 2147483647)
    @Column(name = "name_prod")
    private String nameProd;
    @Size(max = 5)
    @Column(name = "sn_prod")
    private String snProd;
    @JoinColumns({
        @JoinColumn(name = "number_sc", referencedColumnName = "number_", insertable = false, updatable = false)
        , @JoinColumn(name = "name_sc", referencedColumnName = "name_", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    @JsonBackReference
    private ServiceCentr serviceCentr;
    @JoinColumns({
        @JoinColumn(name = "fio", referencedColumnName = "fio", insertable = false, updatable = false)
        , @JoinColumn(name = "phone", referencedColumnName = "phone", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    @JsonBackReference
    private UserOfCentr userOfCentr;

    public Orders() {
    }

    public Orders(OrdersPK ordersPK) {
        this.ordersPK = ordersPK;
    }

    public Orders(OrdersPK ordersPK, BigInteger varranty) {
        this.ordersPK = ordersPK;
        this.varranty = varranty;
    }

    public Orders(int number, String fio, String phone, int numberSc, String nameSc) {
        this.ordersPK = new OrdersPK(number, fio, phone, numberSc, nameSc);
    }

    public OrdersPK getOrdersPK() {
        return ordersPK;
    }

    public void setOrdersPK(OrdersPK ordersPK) {
        this.ordersPK = ordersPK;
    }

    public Date getDateOfReceipt() {
        return dateOfReceipt;
    }

    public void setDateOfReceipt(Date dateOfReceipt) {
        this.dateOfReceipt = dateOfReceipt;
    }

    public Date getDateOfExec() {
        return dateOfExec;
    }

    public void setDateOfExec(Date dateOfExec) {
        this.dateOfExec = dateOfExec;
    }

    public BigInteger getVarranty() {
        return varranty;
    }

    public void setVarranty(BigInteger varranty) {
        this.varranty = varranty;
    }

    public String getTypeRepair() {
        return typeRepair;
    }

    public void setTypeRepair(String typeRepair) {
        this.typeRepair = typeRepair;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getNameProd() {
        return nameProd;
    }

    public void setNameProd(String nameProd) {
        this.nameProd = nameProd;
    }

    public String getSnProd() {
        return snProd;
    }

    public void setSnProd(String snProd) {
        this.snProd = snProd;
    }

    public ServiceCentr getServiceCentr() {
        return serviceCentr;
    }

    public void setServiceCentr(ServiceCentr serviceCentr) {
        this.serviceCentr = serviceCentr;
    }

    public UserOfCentr getUserOfCentr() {
        return userOfCentr;
    }

    public void setUserOfCentr(UserOfCentr userOfCentr) {
        this.userOfCentr = userOfCentr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordersPK != null ? ordersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.ordersPK == null && other.ordersPK != null) || (this.ordersPK != null && !this.ordersPK.equals(other.ordersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vera.restspringdatajpa.model.Orders[ ordersPK=" + ordersPK + " ]";
    }
    
}

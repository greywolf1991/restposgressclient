/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v.e.doronin
 */
@Entity
@Table(name = "service_centr")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceCentr.findAll", query = "SELECT s FROM ServiceCentr s")
    , @NamedQuery(name = "ServiceCentr.findByNumber", query = "SELECT s FROM ServiceCentr s WHERE s.serviceCentrPK.number = :number")
    , @NamedQuery(name = "ServiceCentr.findByAdress", query = "SELECT s FROM ServiceCentr s WHERE s.adress = :adress")
    , @NamedQuery(name = "ServiceCentr.findByName", query = "SELECT s FROM ServiceCentr s WHERE s.serviceCentrPK.name = :name")})
public class ServiceCentr implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ServiceCentrPK serviceCentrPK;
    @Size(max = 2147483647)
    @Column(name = "adress")
    private String adress;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceCentr")
    @JsonBackReference
    private Collection<Orders> ordersCollection;

    public ServiceCentr() {
    }

    public ServiceCentr(ServiceCentrPK serviceCentrPK) {
        this.serviceCentrPK = serviceCentrPK;
    }

    public ServiceCentr(int number, String name) {
        this.serviceCentrPK = new ServiceCentrPK(number, name);
    }

    public ServiceCentrPK getServiceCentrPK() {
        return serviceCentrPK;
    }

    public void setServiceCentrPK(ServiceCentrPK serviceCentrPK) {
        this.serviceCentrPK = serviceCentrPK;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceCentrPK != null ? serviceCentrPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceCentr)) {
            return false;
        }
        ServiceCentr other = (ServiceCentr) object;
        if ((this.serviceCentrPK == null && other.serviceCentrPK != null) || (this.serviceCentrPK != null && !this.serviceCentrPK.equals(other.serviceCentrPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vera.restspringdatajpa.model.ServiceCentr[ serviceCentrPK=" + serviceCentrPK + " ]";
    }
    
}

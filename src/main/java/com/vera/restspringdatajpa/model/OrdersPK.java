/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author v.e.doronin
 */
@Embeddable
public class OrdersPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "number_")
    private int number;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fio")
    private String fio;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="������������ ������ ������ ��������/����� (������ ����� ������ xxx-xxx-xxxx)")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "number_sc")
    private int numberSc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name_sc")
    private String nameSc;

    public OrdersPK() {
    }

    public OrdersPK(int number, String fio, String phone, int numberSc, String nameSc) {
        this.number = number;
        this.fio = fio;
        this.phone = phone;
        this.numberSc = numberSc;
        this.nameSc = nameSc;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getNumberSc() {
        return numberSc;
    }

    public void setNumberSc(int numberSc) {
        this.numberSc = numberSc;
    }

    public String getNameSc() {
        return nameSc;
    }

    public void setNameSc(String nameSc) {
        this.nameSc = nameSc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) number;
        hash += (fio != null ? fio.hashCode() : 0);
        hash += (phone != null ? phone.hashCode() : 0);
        hash += (int) numberSc;
        hash += (nameSc != null ? nameSc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersPK)) {
            return false;
        }
        OrdersPK other = (OrdersPK) object;
        if (this.number != other.number) {
            return false;
        }
        if ((this.fio == null && other.fio != null) || (this.fio != null && !this.fio.equals(other.fio))) {
            return false;
        }
        if ((this.phone == null && other.phone != null) || (this.phone != null && !this.phone.equals(other.phone))) {
            return false;
        }
        if (this.numberSc != other.numberSc) {
            return false;
        }
        if ((this.nameSc == null && other.nameSc != null) || (this.nameSc != null && !this.nameSc.equals(other.nameSc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vera.restspringdatajpa.model.OrdersPK[ number=" + number + ", fio=" + fio + ", phone=" + phone + ", numberSc=" + numberSc + ", nameSc=" + nameSc + " ]";
    }
    
}

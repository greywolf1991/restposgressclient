/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v.e.doronin
 */
@Entity
@Table(name = "user_of_centr")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserOfCentr.findAll", query = "SELECT u FROM UserOfCentr u")
    , @NamedQuery(name = "UserOfCentr.findByFio", query = "SELECT u FROM UserOfCentr u WHERE u.userOfCentrPK.fio = :fio")
    , @NamedQuery(name = "UserOfCentr.findByPhone", query = "SELECT u FROM UserOfCentr u WHERE u.userOfCentrPK.phone = :phone")
    , @NamedQuery(name = "UserOfCentr.findByDateOfBirthsday", query = "SELECT u FROM UserOfCentr u WHERE u.dateOfBirthsday = :dateOfBirthsday")})
public class UserOfCentr implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserOfCentrPK userOfCentrPK;
    @Column(name = "date_of_birthsday")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirthsday;
    @OneToMany(mappedBy = "userOfCentr")
    @JsonBackReference
    private Collection<Prod> prodCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userOfCentr")
    @JsonBackReference
    private Collection<Orders> ordersCollection;

    public UserOfCentr() {
    }

    public UserOfCentr(UserOfCentrPK userOfCentrPK) {
        this.userOfCentrPK = userOfCentrPK;
    }

    public UserOfCentr(String fio, String phone) {
        this.userOfCentrPK = new UserOfCentrPK(fio, phone);
    }

    public UserOfCentrPK getUserOfCentrPK() {
        return userOfCentrPK;
    }

    public void setUserOfCentrPK(UserOfCentrPK userOfCentrPK) {
        this.userOfCentrPK = userOfCentrPK;
    }

    public Date getDateOfBirthsday() {
        return dateOfBirthsday;
    }

    public void setDateOfBirthsday(Date dateOfBirthsday) {
        this.dateOfBirthsday = dateOfBirthsday;
    }

    @XmlTransient
    public Collection<Prod> getProdCollection() {
        return prodCollection;
    }

    public void setProdCollection(Collection<Prod> prodCollection) {
        this.prodCollection = prodCollection;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userOfCentrPK != null ? userOfCentrPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserOfCentr)) {
            return false;
        }
        UserOfCentr other = (UserOfCentr) object;
        if ((this.userOfCentrPK == null && other.userOfCentrPK != null) || (this.userOfCentrPK != null && !this.userOfCentrPK.equals(other.userOfCentrPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vera.restspringdatajpa.model.UserOfCentr[ userOfCentrPK=" + userOfCentrPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author v.e.doronin
 */
@Entity
@Table(name = "prod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prod.findAll", query = "SELECT p FROM Prod p")
    , @NamedQuery(name = "Prod.findByName", query = "SELECT p FROM Prod p WHERE p.prodPK.name = :name")
    , @NamedQuery(name = "Prod.findBySn", query = "SELECT p FROM Prod p WHERE p.prodPK.sn = :sn")
    , @NamedQuery(name = "Prod.findByModel", query = "SELECT p FROM Prod p WHERE p.prodPK.model = :model")
    , @NamedQuery(name = "Prod.findByDateOfManufacture", query = "SELECT p FROM Prod p WHERE p.dateOfManufacture = :dateOfManufacture")
    , @NamedQuery(name = "Prod.findByManufacturerCountry", query = "SELECT p FROM Prod p WHERE p.manufacturerCountry = :manufacturerCountry")})
public class Prod implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProdPK prodPK;
    @Column(name = "date_of_manufacture")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfManufacture;
    @Size(max = 50)
    @Column(name = "manufacturer_country")
    private String manufacturerCountry;
    @JoinColumns({
        @JoinColumn(name = "fio", referencedColumnName = "fio")
        , @JoinColumn(name = "phone", referencedColumnName = "phone")})
    @ManyToOne
    @JsonBackReference
    private UserOfCentr userOfCentr;

    public Prod() {
    }

    public Prod(ProdPK prodPK) {
        this.prodPK = prodPK;
    }

    public Prod(String name, String sn, String model) {
        this.prodPK = new ProdPK(name, sn, model);
    }

    public ProdPK getProdPK() {
        return prodPK;
    }

    public void setProdPK(ProdPK prodPK) {
        this.prodPK = prodPK;
    }

    public Date getDateOfManufacture() {
        return dateOfManufacture;
    }

    public void setDateOfManufacture(Date dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public String getManufacturerCountry() {
        return manufacturerCountry;
    }

    public void setManufacturerCountry(String manufacturerCountry) {
        this.manufacturerCountry = manufacturerCountry;
    }

    public UserOfCentr getUserOfCentr() {
        return userOfCentr;
    }

    public void setUserOfCentr(UserOfCentr userOfCentr) {
        this.userOfCentr = userOfCentr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prodPK != null ? prodPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prod)) {
            return false;
        }
        Prod other = (Prod) object;
        if ((this.prodPK == null && other.prodPK != null) || (this.prodPK != null && !this.prodPK.equals(other.prodPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vera.restspringdatajpa.model.Prod[ prodPK=" + prodPK + " ]";
    }
    
}

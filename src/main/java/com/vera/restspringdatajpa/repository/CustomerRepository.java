package com.vera.restspringdatajpa.repository;


import com.vera.restspringdatajpa.config.DataConfig;
import com.vera.restspringdatajpa.model.Customer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//@Repository
//public interface CustomerRepository<C> extends CrudRepository<Customer, Integer>{
//    List<Customer> findByName(String name);
//}

@Stateless
@Repository("customerRepository")
@Path("com.vera.restspringdatajpa.model.Customer")
public class CustomerRepository implements AbstractRepository<Customer> {
    
    //@Autowired(required = false)
    @PersistenceUnit(unitName= DataConfig.PERSISTANCE_UNIT_NAME)
    private EntityManagerFactory entityManagerFactory;
    
    @Override
    public long save(Customer order) {
        order = entityManagerFactory.createEntityManager().find(Customer.class, order);
        return order.getCustomerId();
    }

    @Override
    public Customer get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Customer> list() {
      CriteriaQuery<Customer> 
      cq = entityManagerFactory.getCriteriaBuilder().createQuery(Customer.class);
      Root<Customer> root = cq.from(Customer.class);
      cq.select(root);
      TypedQuery<Customer> query = getEntityManager().createQuery(cq);
      return query.getResultList();
    }

    @Override
    public void update(long id, Customer order) {
      //Session session = getSessionFactory().getCurrentSession();
      //Customer order2 = session.byId(Customer.class).load(id);
      //book2.setTitle(Order.getTitle());
      // book2.setAuthor(Order.getAuthor());
      //session.flush();
    }

    @Override
    public void delete(long id) {
      //Session session = getSessionFactory().getCurrentSession();
      //Customer order = session.byId(Customer.class).load(id);
      //session.delete(order);
    }

    @Override
    public Customer getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Customer> findByName(String name) {
        //Session session = getSessionFactory().getCurrentSession();
        TypedQuery<Customer> query = getEntityManager().createQuery("SELECT c FROM Customer c where c.name LIKE :nameCustomer", Customer.class)
                .setParameter("nameCustomer", "%" + name + "%");
        return query.getResultList();
    }
    
//    SessionFactory getSessionFactory()
//    {
//        Session session = getEntityManager().unwrap(Session.class);
//        return session.getSessionFactory();
//    }
    @Override
    public EntityManager getEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public List<Customer> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

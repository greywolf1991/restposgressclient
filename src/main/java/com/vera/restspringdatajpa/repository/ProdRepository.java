/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.repository;

import com.vera.restspringdatajpa.config.DataConfig;
import com.vera.restspringdatajpa.model.Prod;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;
import org.springframework.stereotype.Repository;

/**
 *
 * @author v.e.doronin
 */
//@Stateless
@Repository("prodRepository")
//@Path("com.vera.restspringdatajpa.model.Orders")
public class ProdRepository implements AbstractRepository<Prod>{

    @PersistenceUnit(unitName= DataConfig.PERSISTANCE_UNIT_NAME)
    private EntityManagerFactory entityManagerFactory;
    
    
    @Override
    public long save(Prod object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Prod get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, Prod object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Prod getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Prod> findByName(String name) {
        TypedQuery<Prod> query = getEntityManager().
                createQuery("SELECT p FROM Prod p where p.prodPK.name LIKE :name", Prod.class).setParameter("name", "%" + name + "%"); 
        return query.getResultList();
    }
    
    @Override
    public List<Prod> join() {
        Query query = getEntityManager().
                createQuery("FROM Prod AS p inner join p.userOfCentr.ordersCollection AS ord");
        //List<?> list = query.getResultList();
        return query.getResultList();
    }

    @Override
    public List<Prod> list() {
        CriteriaQuery<Prod> 
      cq = entityManagerFactory.getCriteriaBuilder().createQuery(Prod.class);
      Root<Prod> root = cq.from(Prod.class);
      cq.select(root);
      TypedQuery<Prod> query = getEntityManager().createQuery(cq);
      return query.getResultList();
    }

    @Override
    public void delete(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public EntityManager getEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author v.e.doronin
 */
public interface AbstractRepository<T> {
    
    public long save(T object);

    public T get(long id);

    public void update(long id, T object);
    
    public T getById(Integer id);

    public List<T> findByName(String name);
    public List<T> list();
    
    public void delete(long id);

    public List<T> join();
    
    EntityManager getEntityManager();
    
}

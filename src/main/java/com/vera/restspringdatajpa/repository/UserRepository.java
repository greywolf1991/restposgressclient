/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.repository;

import com.vera.restspringdatajpa.config.DataConfig;
import com.vera.restspringdatajpa.model.UserOfCentr;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

/**
 *
 * @author v.e.doronin
 */

@Repository("userRepository")
public class UserRepository implements AbstractRepository<UserOfCentr>{

    
    @PersistenceUnit(unitName= DataConfig.PERSISTANCE_UNIT_NAME)
    private EntityManagerFactory entityManagerFactory;
    
    @Override
    public long save(UserOfCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserOfCentr get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, UserOfCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserOfCentr getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UserOfCentr> findByName(String name) {
        TypedQuery<UserOfCentr> query = getEntityManager().
                createQuery("SELECT u FROM UserOfCentr u where u.userOfCentrPK.fio LIKE :fio", UserOfCentr.class).setParameter("fio", "%" + name + "%"); 
        return query.getResultList();
    }

    @Override
    public List<UserOfCentr> list() {
        CriteriaQuery<UserOfCentr> 
      cq = entityManagerFactory.getCriteriaBuilder().createQuery(UserOfCentr.class);
      Root<UserOfCentr> root = cq.from(UserOfCentr.class);
      cq.select(root);
      TypedQuery<UserOfCentr> query = getEntityManager().createQuery(cq);
      return query.getResultList();
    }

    @Override
    public void delete(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UserOfCentr> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.repository;

import com.vera.restspringdatajpa.config.DataConfig;
import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.model.Orders;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author v.e.doronin
 */

@Stateless
@Repository("ordersRepository")
@Path("com.vera.restspringdatajpa.model.Orders")
public class OrdersRepository implements AbstractRepository<Orders>{
    
    //@Autowired
    @PersistenceUnit(unitName= DataConfig.PERSISTANCE_UNIT_NAME)
    private EntityManagerFactory entityManagerFactory;
    
    @Override
    public long save(Orders object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Orders get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Orders> list() {
        CriteriaQuery<Orders> 
      cq = entityManagerFactory.getCriteriaBuilder().createQuery(Orders.class);
      Root<Orders> root = cq.from(Orders.class);
      cq.select(root);
      TypedQuery<Orders> query = getEntityManager().createQuery(cq);
      return query.getResultList();
    }

    @Override
    public void update(long id, Orders order) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Orders getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Orders> findByName(String name) {
        TypedQuery<Orders> query = getEntityManager().createQuery("SELECT o FROM Orders o where o.ordersPK.nameSc LIKE :nameSC", Orders.class).setParameter("nameSC", "%" + name + "%");
        return query.getResultList();
    }
    
    @Override
    public EntityManager getEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public List<Orders> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

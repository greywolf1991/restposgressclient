/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.repository;

import com.vera.restspringdatajpa.config.DataConfig;
import com.vera.restspringdatajpa.model.ServiceCentr;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

/**
 *
 * @author v.e.doronin
 */
@Repository("centrRepository")
public class CentrRepository implements AbstractRepository<ServiceCentr>{

    
    @PersistenceUnit(unitName= DataConfig.PERSISTANCE_UNIT_NAME)
    private EntityManagerFactory entityManagerFactory;
    
    @Override
    public long save(ServiceCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServiceCentr get(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(long id, ServiceCentr object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServiceCentr getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ServiceCentr> findByName(String name) {
        TypedQuery<ServiceCentr> query = getEntityManager().
                createQuery("SELECT s FROM ServiceCentr s where s.serviceCentrPK.name LIKE :name", ServiceCentr.class).setParameter("name", "%" + name + "%"); 
        return query.getResultList();
    }

    @Override
    public List<ServiceCentr> list() {
        CriteriaQuery<ServiceCentr> 
      cq = entityManagerFactory.getCriteriaBuilder().createQuery(ServiceCentr.class);
      Root<ServiceCentr> root = cq.from(ServiceCentr.class);
      cq.select(root);
      TypedQuery<ServiceCentr> query = getEntityManager().createQuery(cq);
      return query.getResultList();
    }

    @Override
    public void delete(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ServiceCentr> join() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public EntityManager getEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }
    
}

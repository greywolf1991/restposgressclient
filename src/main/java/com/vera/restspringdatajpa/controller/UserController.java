/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.controller;

import com.vera.restspringdatajpa.model.UserOfCentr;
import com.vera.restspringdatajpa.sevicies.AbstractService;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author v.e.doronin
 */
@RestController
@RequestMapping("/user")
public class UserController {
    
    
    @Autowired
    @Qualifier("userService")
    AbstractService userService;


    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return " this is userService ";
    }
    
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/ByFIO/{name}", method = RequestMethod.GET)
    public List<UserOfCentr> getUserByFIO(@PathVariable String name) {
        return userService.findByName(name);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserOfCentr> getAll() {
        return userService.getAll();
    }
    
    @RequestMapping(value = "/join", method = RequestMethod.GET)
    public List<UserOfCentr> joinWith() {
        return userService.join();
    }
    
    
}

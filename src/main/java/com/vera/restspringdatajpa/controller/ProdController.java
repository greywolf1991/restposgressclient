/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.controller;

import com.vera.restspringdatajpa.model.Prod;
import com.vera.restspringdatajpa.sevicies.AbstractService;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author v.e.doronin
 */
@RestController
@RequestMapping("/prod")
public class ProdController {
    
    @Autowired
    @Qualifier("prodService")
    AbstractService prodService;


    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return " this is prodService ";
    }
    
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/ByName/{name}", method = RequestMethod.GET)
    public List<Prod> getProdByName(@PathVariable String name) {
        return prodService.findByName(name);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Prod> getAll() {
        return prodService.getAll();
    }
    
    @RequestMapping(value = "/join", method = RequestMethod.GET)
    public List<Prod> joinWithOrders() {
        return prodService.join();
    }

    @RequestMapping(value = "/byID/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteProd(@PathVariable Integer id) {
        prodService.delete(id);
        return HttpStatus.NO_CONTENT;
    }
    
}

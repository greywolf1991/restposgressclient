package com.vera.restspringdatajpa.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.sevicies.AbstractService;
import com.vera.restspringdatajpa.sevicies.CustomerService;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    @Qualifier("customerService")
    AbstractService customerService;

    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return "this is customer";
    }

    //@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
    //public @ResponseBody
    //Customer getAllCustomer(@PathVariable Integer id) {
    //    return customerService.getById(id);
    //}
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/byName/{name}", method = RequestMethod.GET)
    public List<Customer> getCustomerByName(@PathVariable String name) {
        return customerService.findByName(name);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Customer> getAll() {
        return customerService.getAll();
    }

    @RequestMapping(value = "/byID/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteCustomer(@PathVariable Integer id) {
        customerService.delete(id);
        return HttpStatus.NO_CONTENT;
    }

    //@RequestMapping(value = "/customer", method = RequestMethod.POST)
    //public HttpStatus insertCustomer(@RequestBody Customer customer) {
    //    return customerService.addCustomer(customer) ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
    //}

    //@RequestMapping(value = "/customer", method = RequestMethod.PUT)
    //public HttpStatus updateCustomer(@RequestBody Customer customer) {
    //    return customerService.updateCustomer(customer) ? HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
    //}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.controller;

import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.model.Orders;
import com.vera.restspringdatajpa.sevicies.AbstractService;
import com.vera.restspringdatajpa.sevicies.OrdersService;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author v.e.doronin
 */
@RestController
@RequestMapping("/orders")
public class OrdersController {
    
    @Autowired
    @Qualifier("ordersService")
    AbstractService ordersService;


    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return " this is orders ";
    }
    
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/ByNameSC/{name}", method = RequestMethod.GET)
    public List<Orders> getOrderByName(@PathVariable String name) {
        return ordersService.findByName(name);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Orders> getAll() {
        List<Orders> o = ordersService.getAll();
        return ordersService.getAll();
    }

    @RequestMapping(value = "/byID/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteOrder(@PathVariable Integer id) {
        ordersService.delete(id);
        return HttpStatus.NO_CONTENT;
    }
}

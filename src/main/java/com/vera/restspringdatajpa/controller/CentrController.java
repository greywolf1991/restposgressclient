/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.controller;

import com.vera.restspringdatajpa.model.ServiceCentr;
import com.vera.restspringdatajpa.sevicies.AbstractService;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author v.e.doronin
 */

@RestController
@RequestMapping("/servicecentr")
public class CentrController {
    
    @Autowired
    @Qualifier("centrService")
    AbstractService centrService;
    
    
    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return " this is centrService ";
    }
    
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/ByName/{name}", method = RequestMethod.GET)
    public List<ServiceCentr> getCentrByName(@PathVariable String name) {
        return centrService.findByName(name);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ServiceCentr> getAll() {
        return centrService.getAll();
    }
    
    @RequestMapping(value = "/join", method = RequestMethod.GET)
    public List<ServiceCentr> joinWith() {
        return centrService.join();
    }
    
}
